#include<stdio.h>
int main()
{
  	int num, i;
  
  	printf("\n Please Enter the bigger value: ");
  	scanf("%d", &num);
  	
  	printf("\n List of Numbers from %d to 1 are \n", num);  	
	for(i = num; i >= 1; i--)
  	{
    	printf(" %d \t", i);
  	}
  
  	return 0;
}